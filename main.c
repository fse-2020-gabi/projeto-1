#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <math.h>

#include "menu/menu.h"
#include "modbus/modbus.h"
#include "bme280/bme280.h"
#include "gpio/gpio.h"
#include "lcd/lcd.h"
#include "pid/pid.h"
#include "csv/csv.h"

#define KP 5
#define KI 1
#define KD 5

int hist_ventoinha;
double pid = 0;
int id_uart;
float tr;
float ti;
float te;
int op;

void controlaTemperatura();
void iniciaJanelaMenu();
void loop();
void acionaMenu();
void intSignalHandler(int signal);
void trataInterrupcao(int signal);


int main(){

  signal(SIGINT, intSignalHandler);
  signal(SIGKILL, trataInterrupcao);
  signal(SIGTSTP, trataInterrupcao);
  
  acionaMenu();
  cria_csv();
  loop();

  return 0;
}

void trataInterrupcao(int signal){
  printf("Adeus!\n");
  
  desligarDispositivo(VENTOINHA_PIN);
  desligarDispositivo(RESISTOR_PIN);
  close(id_uart);
  sleep(1);
  exit(0);
}

void intSignalHandler(int signal){
  clear();
  printf("\n\nRetornando para o Menu\n\n");
  acionaMenu();
}

void acionaMenu(){
  
  op = mostrarMenu();
  if(op == OP_DIG){
    scanf("%f", &tr);
  }
  hist_ventoinha = definir_histerese();

}

void controlaTemperatura(){

  if(hist_ventoinha == 0 || fabs(tr - ti) > hist_ventoinha){
    if(tr < te){
      clear();
      printf("!!! IMPOSSÍVEL CHEGAR À TEMPERATURA DE REFERÊNCIA !!!\n\n");
      main();
    } else {
      pid = pid_controle(ti);
      setPID(pid);
    }

  }

}

void loop() {

  int count = 0;
  
  id_uart = abre_uart();
  int fd = abreI2C();
  iniciarLcd();
  iniciaGPIO();
  pid_configura_constantes(KP, KI, KD);
  pid_atualiza_referencia(tr);

  while(1){
    if(op == OP_POT){
      tr = solicita_temp(id_uart, TEMP_POT);
      pid_atualiza_referencia(tr);
    }

    ti = solicita_temp(id_uart, TEMP_INTERNA);
    te = solicitaTE(fd);

    clear();
    mostraTemperaturas(ti, tr, te);
    controlaTemperatura();
    
    if(count == 1){
      if(pid < MIN_VENTOINHA || pid >= 0){
        adiciona_linha(pid, tr, te, ti);
      } else {
        adiciona_linha(0, tr, te, ti);
      }
      count = 0;
    } else {
      count = 1;
    }

    usleep(100000);
  }

}
