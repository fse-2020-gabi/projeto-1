run: main.c menu/menu.c gpio/gpio.c bme280/bme280.c lcd/lcd.c modbus/modbus.c modbus/crc/crc16.c pid/pid.c csv/csv.c
	gcc main.c menu/menu.c gpio/gpio.c bme280/bme280.c lcd/lcd.c modbus/modbus.c modbus/crc/crc16.c pid/pid.c csv/csv.c -lwiringPi -lm -lbcm2835 -o bin/main

start: bin/main
	bin/main