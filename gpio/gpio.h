#define RESISTOR_PIN 4 //GPIO 23 ou Pino 16 (Board)
#define VENTOINHA_PIN 5 //GPIO 24 ou Pino 18 (Board)
#define MIN_VENTOINHA -40

void desligarDispositivo(int dispositivo);
void ligarDispositivo(int dispositivo, int intensidade);
void setPID(float pid);
void iniciaGPIO();
