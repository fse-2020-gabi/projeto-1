#include <stdio.h>    
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

#include <wiringPi.h>
#include <softPwm.h> 

#include "gpio.h"

void configuraPinos(){
}

void desligarDispositivo(int dispositivo){
  softPwmWrite(dispositivo, 0);
}

void ligarDispositivo(int dispositivo, int intensidade){
  softPwmWrite(dispositivo, intensidade);
  if(dispositivo == VENTOINHA_PIN){
    return desligarDispositivo(RESISTOR_PIN);
  }

  desligarDispositivo(VENTOINHA_PIN);
}

void setPID(float pid){
  if(pid < MIN_VENTOINHA){
    ligarDispositivo(VENTOINHA_PIN, fabs(pid));
    desligarDispositivo(RESISTOR_PIN);
  } else if(pid > 0) {
    ligarDispositivo(RESISTOR_PIN, pid);
    desligarDispositivo(VENTOINHA_PIN);
  } else {
    desligarDispositivo(VENTOINHA_PIN);
    desligarDispositivo(RESISTOR_PIN);
  }

}

// MARK: BME280

void iniciaDispositivo(int pin){
	pinMode(pin, OUTPUT);	/* set GPIO as output */
	softPwmCreate(pin, 1, 100);
}

void iniciaGPIO(){
	wiringPiSetup();		/* initialize wiringPi setup */

  iniciaDispositivo(VENTOINHA_PIN);
  iniciaDispositivo(RESISTOR_PIN);
}

/*
  Control Intensity of LED using PWM on Raspberry pi
  http://www.electronicwings.com
 */