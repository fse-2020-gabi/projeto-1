#include <stdio.h>
#include <stdlib.h>

#include "menu.h"

int mostrarMenu(){
  int opcao;

  printf("\n\nOlá! Seja bem-vindo(a).\n\n");
  printf("Primeiro, escolha como você deseja que Temperatura de Referência (TR) seja definida:\n\n\n");
  printf("1 - Pelo Potenciomêtro \n\n2 - Digitar a Temperatura\n\n\n");

  opcao = getchar();

  switch (opcao) {
    case '1':
      printf("Você escolheu o potenciômetro!\n\n");
      return OP_POT;

    case '2':
      printf("Escreva qual a TR você deseja: \n\n");
      return OP_DIG;

    default:
      clear();
      printf("\n\n!!! Você deve escolher uma opção válida !!!\n\n");
      return mostrarMenu();
  }
  return opcao;
}

int definir_histerese(){

  int hv;
  printf("\n\nVocê deseja definir o valor de histerese da ventoninha?\nCaso sim, digite o valor\nCaso não, digite 0 \n\n");
  scanf("%d", &hv);

  if(hv < 0) {
    printf("\n\n!!! Você deve escolher um valor maior que 0 !!!\n\n");

    return definir_histerese();
  };
  
  return hv;

}