#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>         //Used for UART
#include <fcntl.h>          //Used for UART
#include <termios.h>        //Used for UART

#include "crc/crc16.h"
#include "modbus.h"

int uart0_filestream = -1;

// MARK: utils

void imprimir_array_uchar(unsigned char * array, char* titulo, int size){

  printf("%s", titulo);
  for (int i = 0; i < size; i++){
    printf("%d", array[i]);
  }
  printf("\n");

}

// MARK: ações UART
int abre_uart(){
  uart0_filestream = open("/dev/serial0", O_RDWR | O_NOCTTY | O_NDELAY);

  if (uart0_filestream == -1){
    printf("ERRO AO INICAR UART.\n");
  }
  
  struct termios options;
  tcgetattr(uart0_filestream, &options);
  options.c_cflag = B9600 | CS8 | CLOCAL | CREAD;     //<Set baud rate
  options.c_iflag = IGNPAR;
  options.c_oflag = 0;
  options.c_lflag = 0;
  tcflush(uart0_filestream, TCIFLUSH);
  tcsetattr(uart0_filestream, TCSANOW, &options);

  return uart0_filestream;
}

void escreve_uart(int uart0_filestream, unsigned char * mensagem, int tamanho_msg, unsigned char option, unsigned char sub_option){
  
  int size_codigo = MENS_DEFAULT_SIZE + tamanho_msg;
  int arg_size = size_codigo + CRC_SIZE;

  unsigned char args[arg_size];

  unsigned char default_msg[MENS_DEFAULT_SIZE] = {COD_DIS, option, sub_option};
  memcpy(args, default_msg, MENS_DEFAULT_SIZE);
  if (tamanho_msg != 0){
    memcpy(&args[MENS_DEFAULT_SIZE], mensagem, tamanho_msg);
  }
  short crc = calcula_CRC(args, size_codigo);
  memcpy(&args[size_codigo], &crc, CRC_SIZE);
  
  int count = write(uart0_filestream, args, arg_size);

  if (count < 0){
    printf("ERRO AO ESCREVER NA UART\n");

  }
}

void le_uart(int uart0_filestream, unsigned char * src, int responseSize){
  // printf("LENDO UART\n");
  unsigned char response[256];
  int size = read(uart0_filestream, response, responseSize);
  if (size < 0){
    printf("ERRO AO LER DADOS.\n");

    sleep(1);
    return le_uart(uart0_filestream, src, responseSize);
  }
  
  if (size == 0){
    printf("NENHUM DADO DISPONÍVEL.\n");

    usleep(500);
    return le_uart(uart0_filestream, src, responseSize);
  }

  memcpy(src, response, size);
  return;
}

float verificaCrcRecebido(unsigned char * msg_rec, unsigned char * crc, int uart0_filestream, int tipo_temp){

  short crc_comp = calcula_CRC(msg_rec, 7);

  short crc_rec;
  memcpy(&crc_rec, crc, CRC_SIZE);

  if(crc_comp == crc_rec){
    float tr;
    memcpy(&tr, &msg_rec[3], MATRICULA_S);
    return tr;
  } else {
    printf("CRC ESPERADO: %hi\n CRC RECEBIDO: %hi\n\n", crc_comp, crc_rec);

    return solicita_temp(uart0_filestream, tipo_temp);
  }
}

//MARK: Projeto 1

float solicita_temp(int uart0_filestream, int tipo_temp){
    unsigned char mensagem[MATRICULA_S] = {0x01, 0x08, 0x01, 0x07};

  if(tipo_temp == TEMP_POT){
    escreve_uart(uart0_filestream, mensagem, MATRICULA_S, COD_SOLICITACAO, COD_LE_POTENCIOMETRO);
  } else {
    escreve_uart(uart0_filestream, mensagem, MATRICULA_S, COD_SOLICITACAO, COD_LE_TEMP_INT);
  }
  usleep(400000);
  unsigned char response[RES_S]; 
  le_uart(uart0_filestream, response, RES_S);
  
  unsigned char crc_rec[2];
  unsigned char msg_rec[7];
  
  memcpy(msg_rec, response, 7);
  memcpy(crc_rec, &response[7], CRC_SIZE);

  return verificaCrcRecebido(msg_rec, crc_rec, uart0_filestream, tipo_temp);
  
}

// scp -P 23900 main.c gabrielasilva@3.tcp.ngrok.io:~/exercicio2
// ssh gabrielasilva@3.tcp.ngrok.io -p 23900

// 160121817