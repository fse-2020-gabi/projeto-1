#define COD_DIS 0x01
#define COD_SOLICITACAO 0x23

#define COD_LE_TEMP_INT 0xC1 // TI
#define COD_LE_POTENCIOMETRO 0xC2 // TR

#define CRC_SIZE 2
#define MENS_DEFAULT_SIZE 3

#define MATRICULA_S 4
#define MATRICULA 0x719

#define RES_S 9

#define TEMP_INTERNA 0
#define TEMP_POT 1

int abre_uart();
float solicita_temp(int uart0_filestream, int tipo_temp);
