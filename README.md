# Projeto 1 - FSE

Nome: Gabriela Medeiros da Silva

Matrícula: 16/0121817

## Instruções de Uso

### Instalações iniciais

Instale o Makefile. Para isso, execute o seguinte comando em seu terminal:

``` 
$ sudo apt-get install make 
```

Crie uma pasta chamada "bin" na raíz do projeto

``` 
$ mkdir bin 
```

### Compilando e inicando o programa

Use o Makefile para executar os comandos de compilação e execução do programa:

```
$ make
```
```
$ make start
```

### Programa em  execução

Assim que o programa iniciar, você verá um menu com 2 opções de seleção de temperatura.

```
Primeiro, escolha como você deseja que Temperatura de Referência (TR) seja definida:

1 - Pelo Potenciomêtro 
2 - Digitar a Temperatura 
```

Escolha a opção que mais lhe interessa.
Após isso, insira o valor de histerese da ventoinha.

[O que é histerese?](https://blog.ageon.com.br/o-que-e-histerese-em-um-controlador-de-temperatura/)

```
Você deseja definir o valor de histerese da ventoninha?
Caso sim, digite o valor
Caso não, digite 0 
```

Após inserir o valor de seu interesse, o programa entrará em seu _loop_ de execução. Nele você terá 2 _feedbacks_:

1. As informações de cada temperatura (interna, externa e de referência) atualizada a cada 1 segundo pelo terminal e

2. Um arquivo CSV sendo atualizado a cada 2 segundos, fornecendo a informação de data, hora, TI, TE, TR e intensidade de execução da ventoinha e do resitor (valor de -100 a 100).

*Obs.: Esse arquivo possui o nome "projeto1.csv" e poderá ser encontrado na pasta raíz do projeto*

### Interromper execução

Se você deseja voltar ao menu e informar outros valores, aperte ``` ctrl + c ```.
Se Você deseja finalizar o programa, aperte ``` ctrl + z ```.

### Resultados

A seguir são mostrados os resultados do experimento em formato de gráficos de linha com dados obtidos durante 10 minutos de execução.

O primeiro mostra as alterações das temperaturas de referência, externa e interna ao longo do tempo.

O segundo mostra a intensidade em que os dispositivos (ventoinha e resistor) foram ligados durante esse período.

![[Gráfico 1](graph/newplot1.png)](graph/newplot1.png)

![[Gráfico 2](graph/newplot2.png)](graph/newplot2.png)