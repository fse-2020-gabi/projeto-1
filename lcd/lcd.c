#include <wiringPiI2C.h>
#include <wiringPi.h>
#include <stdlib.h>
#include <stdio.h>

#include "lcd.h"

#define I2C_ADDR 0x27

#define LCD_DATA  1
#define LCD_COMMAND  0 
#define LCD_BACKLIGHT 0x08

#define LINE1 0x80 // 1st line
#define LINE2 0xC0 // 2nd line

#define ENABLE 0b00000100 // Enable bit

void lcd_init(void);
void lcd_byte(int bits, int mode);
void lcd_toggle_enable(int bits);

void typeFloat(float myFloat);
void lcdLoc(int line);
void clearLcd(void); // clr LCD return home
void typeln(const char *s);
int fd;

// Alteração Gabriela Medeiros da Silva

void iniciarLcd(){

  if (wiringPiSetup () == -1) {
    exit (1);
  };
  fd = wiringPiI2CSetup(I2C_ADDR);
  lcd_init();
}

void mostraTemperaturas(float ti, float tr, float te){

  printf("---------- DADOS ATUALIZADOS ---------\n\n");
  printf("TR: %.1f\n", tr);
  printf("TI: %.1f\n", ti);
  printf("TE: %.1f\n", te);
  printf("--------------------------------------\n\n");
  printf("Caso deseje voltar ao menu, pressione CTRL + C \n");
  printf("--------------------------------------\n\n");
  
  lcdLoc(LINE1);
  typeln("TR:");
  typeFloat(tr);
  typeln(" TI:");
  typeFloat(ti);

  lcdLoc(LINE2);
  typeln("TE:");
  typeFloat(te);
}

// Final da Alteração

void typeFloat(float value)   {
  char buffer[20];
  sprintf(buffer, "%4.2f",  value);
  typeln(buffer);
}

void clearLcd(void)   {
  lcd_byte(0x01, LCD_COMMAND);
  lcd_byte(0x02, LCD_COMMAND);
}

void lcdLoc(int line)   {
  lcd_byte(line, LCD_COMMAND);
}

void typeln(const char *s)   {
  while ( *s ) lcd_byte(*(s++), LCD_DATA);
}

void lcd_byte(int bits, int mode)   {

  int bits_high;
  int bits_low;
  // uses the two half byte writes to LCD
  bits_high = mode | (bits & 0xF0) | LCD_BACKLIGHT ;
  bits_low = mode | ((bits << 4) & 0xF0) | LCD_BACKLIGHT ;

  // High bits
  wiringPiI2CReadReg8(fd, bits_high);
  lcd_toggle_enable(bits_high);

  // Low bits
  wiringPiI2CReadReg8(fd, bits_low);
  lcd_toggle_enable(bits_low);
}

void lcd_toggle_enable(int bits)   {
  // Toggle enable pin on LCD display
  delayMicroseconds(500);
  wiringPiI2CReadReg8(fd, (bits | ENABLE));
  delayMicroseconds(500);
  wiringPiI2CReadReg8(fd, (bits & ~ENABLE));
  delayMicroseconds(500);
}

void lcd_init()   {
  // Initialise display
  lcd_byte(0x33, LCD_COMMAND); // Initialise
  lcd_byte(0x32, LCD_COMMAND); // Initialise
  lcd_byte(0x06, LCD_COMMAND); // Cursor move direction
  lcd_byte(0x0C, LCD_COMMAND); // 0x0F On, Blink Off
  lcd_byte(0x28, LCD_COMMAND); // Data length, number of lines, font size
  lcd_byte(0x01, LCD_COMMAND); // Clear display
  delayMicroseconds(500);
}