import pandas as pd
import plotly.express as px
import plotly.graph_objects as go

df = pd.read_csv('projeto1.csv')

# fig = px.line(df, x = 'HORARIO', y = ['TE', 'TI', 'TR'])

# fig.update_traces(
#   name='Temp',
#   mode='lines',
#   marker=dict(color='blue', size=5),
#   showlegend=True
# )

# fig.update_layout(template='simple_white',
#   yaxis_title='TEMPERATURAS',
#   title='TEMPERATURAS DURANTE O TEMPO',
# )

fig = go.Figure(go.Scatter(
  x = df['HORARIO'], 
  y = df['TE'],
  name='TE'
))

fig.add_trace(go.Scatter(
  x = df['HORARIO'], 
  y = df['TI'],
  name='TI'
))

fig.add_trace(go.Scatter(
  x = df['HORARIO'], 
  y = df['TR'],
  name='TR'
))

fig.update_layout(
  title='TEMPERATURAS DURANTE O TEMPO',
  plot_bgcolor='#fff',
  showlegend=True
)


fig.show()

# #####

fig = px.line(df, x = 'HORARIO', y = ['PID'])

fig.update_traces(
  name='Resistor/ventoinha',
  mode='lines',
  marker=dict(color='blue', size=5),
  showlegend=True
)

fig.update_layout(template='simple_white',
  yaxis_title='GPIO',
  title='Intensidade dos ispositivos',
)

fig.show()