#include <stdio.h>
#include <string.h>
#include <time.h>

#include "csv.h"

void adiciona_linha(double pid, float tr, float te, float ti){

  FILE *fp; 
  fp = fopen(FILENAME, "a+");

  time_t segundos;
  time(&segundos);
  struct tm *data_hora_atual = localtime(&segundos);

  fprintf(fp, "%d/%d ", data_hora_atual->tm_mday, data_hora_atual->tm_mon+1);
  fprintf(fp, "%d:%d:%d,", data_hora_atual->tm_hour, data_hora_atual->tm_min, data_hora_atual->tm_sec);
  fprintf(fp, "%.2f,%.2f,%.2f,%.2lf\n", tr, te, ti, pid);

  fclose(fp);

}
void cria_csv(){
   
  FILE *fp;  
  fp = fopen(FILENAME, "w+");
  
  fprintf(fp, "HORARIO,TR,TE,TI,PID\n");

  fclose(fp);
 
}